'''Se conoce el termino histograma, sin embargo, no se puede tener una idea clara de como trabaja a nivel de imagen'''

'''Objetivo: Reconocer los canales de color en una imagen, representarlos de una  manera grafica'''

'''Datos Entrada: img. Imagen de la cual se obtendra el histograma'''

'''Datos Salida: his. Histograma de la imagen'''
import cv2
import numpy as np
from matplotlib import pyplot as plt

'''Lee y muestra en pantalla la imagen especificada'''
img = cv2.imread('../Imagenes/lena-clara.jpg')
cv2.imshow('lena', img)

'''Se define una lista: color'''
color = ('b','g','r')

'''Se calcula el histograma de la imagen en los canales RGB, se utiliza la lista: color para asignar el canal al ciclo'''
for i, c in enumerate(color):

    '''Se calcula el histograma y se imprime con el color especificado en color, posteriormente se imprime en pantalla'''
    hist = cv2.calcHist([img], [i], None, [256], [0, 256])
    plt.plot(hist, color = c)
    plt.xlim([0,256])

plt.show()

'''Se cierran todas las ventanas'''
cv2.destroyAllWindows()