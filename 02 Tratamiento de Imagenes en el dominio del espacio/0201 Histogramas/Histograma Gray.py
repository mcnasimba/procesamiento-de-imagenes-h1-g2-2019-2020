'''Se conoce el termino histograma, sin embargo, no se puede tener una idea clara de como trabaja a nivel de imagen'''

'''Objetivo: Reconocer los canales de color en una imagen, representarlos de una  manera grafica'''

'''Datos Entrada: img. Imagen de la cual se obtendra el histograma'''

'''Datos Salida: his. Histograma de la imagen'''

import cv2
import numpy as np
from matplotlib import pyplot as plt

'''Con la ayuda de la funcion: imread, se lee la imagen especificada y se le asigna un espacio en escala de grises'''
img = cv2.imread('../Imagenes/lena-clara.jpg', 0)
'''Se visualiza la imagen: img'''
cv2.imshow('Lena', img)

'''La funcion calchist permite obtener el histograma de una imagen y se almacena en la variable: hist'''
hist = cv2.calcHist([img], [0], None, [256], [0, 256])

'''La funcion plot, permite realizar la grafica a traves de una lista de puntos, la cual es obtenida de hist
 arg color: define el color de la grafica'''
plt.plot(hist, color='gray' )

'''Asigna nombres a los ejex x e y, para su visualizacion'''
plt.xlabel('intensidad de iluminacion')
plt.ylabel('cantidad de pixeles')

'''Se muestra la imagen con la funcion: show'''
plt.show()

cv2.destroyAllWindows()