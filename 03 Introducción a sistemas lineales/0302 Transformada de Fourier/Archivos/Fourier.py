'''Se busca aplicar la transformada de fourier a una imagen y encontrar su espectro'''

'''Objetivo: Encotrar la transformada de Fourier con opencv'''

'''Datos entrada: img: imagen a processar'''

'''Datos Salida:
    img_back: Imagen reconstruida
    magnitude_spectrum: magnitud del espectro de la imagen'''

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

#Se lee la imagen de la direccion especificada y en espacio de escala de grises
img = cv.imread('../Imagenes/it.jpeg',0)

#Se encuentra la transformacion de frecuencia de la imagen
f = np.fft.fft2(img)

#Se traslada el componente de frecuencia 0 al centro de la matriz
fshift = np.fft.fftshift(f)

#Se obtiene el espectro generado por la transformada de Fourier
magnitude_spectrum = 20*np.log(np.abs(fshift))

#Se obtiene la dimension de la imagen
filas, cols = img.shape

#Se redimensiona los datos obtenidos anteriormente
crow, ccol = filas // 2, cols // 2

fshift [crow: crow -32, ccol-10: ccol + 11] = 0

#Se reconstruye la imagen a travez de un filtro
f_ishift = np.fft.ifftshift (fshift)

#Calcula la transformada discreta inversa bidimensional de Fourier.
img_back = np.fft.ifft2 (f_ishift)

#asigna a img_back la parte real de la imagen
img_back = np.real (img_back)

#Presenta en una ventana la imagen original, la magnitud de espectro y la transformada de Fourier para recontruir la imagen
plt.subplot (131), plt.imshow (img, cmap = 'gray' )
plt.title ( 'Imagen de entrada' ), plt.xticks ([]), plt.yticks ([])
plt.subplot (132), plt.imshow (magnitude_spectrum, cmap = 'gray' )
plt.title ( 'magnitude_spectrum' ), plt.xticks ([]), plt.yticks ([])
plt.subplot (133), plt.imshow (img_back)
plt.title ( 'Resultado en JET' ), plt.xticks ([]), plt.yticks ([])
plt.show ()