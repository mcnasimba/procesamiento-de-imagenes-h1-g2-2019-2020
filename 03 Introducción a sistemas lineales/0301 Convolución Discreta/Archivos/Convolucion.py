'''Se espera obtener o detectar los bordes en una imagen y mostrar el resultado de la nueva imagen'''

'''Objetivo: Detectar los bordes en una imagen con un filtro tipo Sobel'''

'''Datos entrada: img. Imagen a ser procesada'''

'''Datos Salida: sobelx8u. Imagen filtro sobel cv2.CV_8u
                 sobelx64f. Imagen filtro sobel cv2.CV64f'''

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('../Imagenes/lena-clara.jpg',0)

# Se obtiene la imagen con filtro tipo sobel de tipo de datos = cv2.CV_8U
sobelx8u = cv2.Sobel(img,cv2.CV_8U,1,0,ksize=5)

#Se obtiene la imagen con filtro tipo sobel de tipo de datos= cv2.CV_64F.
sobelx64f = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
abs_sobel64f = np.absolute(sobelx64f)
sobel_8u = np.uint8(abs_sobel64f)

#Se muestran las imagenes en una ventana, se asigna titulo y espacio a cada una de ellas
plt.subplot(1,3,1),plt.imshow(img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(1,3,2),plt.imshow(sobelx8u,cmap = 'gray')
plt.title('Sobel CV_8U'), plt.xticks([]), plt.yticks([])
plt.subplot(1,3,3),plt.imshow(sobel_8u,cmap = 'gray')
plt.title('Sobel abs(CV_64F)'), plt.xticks([]), plt.yticks([])

plt.show()