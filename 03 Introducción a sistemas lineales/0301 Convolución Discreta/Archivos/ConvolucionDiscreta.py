'''Un pixel se dice que es parte del borde de una imagen si se produce un cambio brusco en su vecindad,
Se busca aplicar una mascara de convolucion para la deteccion de los bordes de una imagen'''

'''Objetivo: Realzar el detalle de los bordes de una imagen'''

'''Datos Entrada: 
                    imagen_original
                    Coeficientes: Matriz de convolucion
'''

'''Datos Salida: 
                    Imagen_Procesada: Imagen despues de aplicar la mascara de convolucion
'''

from PIL import Image, ImageFilter

tamaño = (5, 5)

'''Coeficientes de la matriz de convolucion'''
coeficientes = [0, 0, -1, 0, 0, 0, -1, -2, -1, 0, -1, -2, 16, -2, -1, 0, -1, -2, -1, 0, 0, 0, -1, 0, 0]

factor = 1

imagen_original = Image.open('../Imagenes/lena-clara.jpg')

'''Se aplica el filtro Kernel a la imagen y se crea una variable: imagen_procesada'''
imagen_procesada = imagen_original.filter(ImageFilter.Kernel(tamaño, coeficientes, factor))

'''Se graba el resultado, en la ruta especificada'''
imagen_procesada.save('../Procesadas/lena-claraMod.jpeg')

'''Se muestra la imagen procesada en pantalla'''
imagen_procesada.show()

'''Se cierran ambos objetos creados de la clase Image'''
imagen_original.close()
imagen_procesada.close()