'''Esta clase permite leer una imagen especificada en el codigo y a travez de una operacion de matrices se obtiene su negativo
y lo muestra en pantalla'''

'''Se importa la clase Image de la libreria PIL'''
from PIL import Image

'''Asignamos la imagen especificada en la ruta a la variable foto'''
foto = Image.open('../Imagenes/flores-color.jfif')

'''Se obtiene una matriz con los datos almacenados en la variable: foto'''
datos = list(foto.getdata())

'''Se recorre la lista: datos, y se resta 255 a cada elemento'''
datos_invertidos = [(255 - datos[x][0], 255 - datos[x][1], 255 - datos[x][2]) for x in range(len(datos))]

'''Se crea una nueva variable: imagen_invertida, se le asigna un espacio: RGB y un tamano igual al de la imagen: foto'''
imagen_invertida = Image.new('RGB', foto.size)

'''Se obtienen los datos obtenidos al realizar la resta de matrices y es asignada a la variable: imagen_invertida'''
imagen_invertida.putdata(datos_invertidos)

imagen_invertida.show()

'''Se cierran las imagenes utilizadas en este ejercicio, se libera espacio'''
imagen_invertida.close()
foto.close()