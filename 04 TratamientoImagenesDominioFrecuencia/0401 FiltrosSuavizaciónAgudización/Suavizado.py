from PIL import Image
from PIL import ImageFilter
from matplotlib import pyplot as plt

# Open an already existing image
img = Image.open("Montañas.jpg");

# Apply sharp filter
blur = img.filter(ImageFilter.BLUR);

plt.subplot(121),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(blur),plt.title('Sharpened')
plt.xticks([]), plt.yticks([])
plt.show()